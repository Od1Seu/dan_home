//Обьясните своими словами, как вы понимаете асинхронность в Javascript
// Коли виконується б-який код, є декілька функцій і кожна настпуна функція не виконається доки попередня не відпрацює - це синхронність.
// Ахинсхронність це коли програма не чекає виконання певного функціоналу, а йде далі ))
createBtn()
const getBtn = document.querySelector('.btn');
const url = 'https://api.ipify.org/?format=json';

getBtn.addEventListener('click', getApi);

function createBtn() {
    const btn = document.createElement('button');
    btn.classList.add('btn');
    btn.innerHTML = 'Вычислить по IP';
    btn.style =
        `
            width: 100px;
            height: 40px;
            background-color: tomato;
            border: 1px solid black;
            box-shadow: 1px -2px 3px green;
            outline: none;
            cursor: pointer;
            display: block
        `;
    document.body.appendChild(btn);
}
function getApi (){
   getIp(getInformation);

}
async function getIp(getAllInform) {
    const res = await fetch(url);
    const data = await res.json();
    getAllInform(data.ip)

}
async function getInformation(api) {
    const res = await fetch(`http://ip-api.com/json/${api}`);
    const data = await res.json();
    createList(data)

}
function createList(data) {
   const div = document.createElement('div');
    div.innerHTML =
   `
    <h2>IP: ${data.query}</h2>
    <h2>Country: ${data.country}</h2>
    <h2>City: ${data.city}</h2>
    <p>RegionName: ${data.regionName}</p>
    <p>Region: ${data.region}</p>
    <p>Timezone: ${data.timezone}</p>
   `;
    getBtn.appendChild(div);
}
