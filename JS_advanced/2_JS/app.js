//Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
// Тоді коли ми програмуємо функцію та метод, який може привести до логічної помилки та ми розуміли з чим працювати чи
// відслідклвувати запрос на сервер через fetch

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
createList(books);
function createList(arr) {
    arr.forEach(item => {
      try{
        checkBooks(item)
      }catch (e) {
          console.error(e.name +' '+e.message)
      }
    })
}
function checkBooks(books) {
    const ul = document.createElement('ul');
    const div = document.getElementById('root');
    div.append(ul);
    if('name' in books && 'author' in books && 'price' in books){
                let li  = document.createElement('li');
                li.textContent = `
                Author : ${books.author},
                Name : ${books.name},
                Price : ${books.price},
                  `;
                ul.append(li)
            }else{
                    if(!('author' in books)) throw new Error('Нет свойства в масиве : author');
                    if(!('price' in books)) throw new Error('Нет свойства в масиве : price');
                    if(!('name' in books)) throw new Error('Нет свойства в масиве : name');
            }
}
