//Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// Це коли є обєкт зі своїми параметрами, і ми можемо повторно використовувати його функціонал через наслідування.
class Employee {
    constructor(option){
        this.name = option.name;
        this.age = option.age;
        this.salary = option.salary;
    }
    get takeName(){
        return `You name is ${this.name}.`
    }
    set takeName(value){
        console.log(value)
    }
    get takeAge(){
        return `You name is ${this.age}.`
    }
    set takeAge(value){
        console.log(value)
    }
    get takeSalary(){
        return `You name is ${this.salary}.`
    }
    set takeSalary(value){
        console.log(value)
    }
}
class Programmer extends Employee {
    constructor(option){
        super(option);
        this.lang = [option.lang];
    }
    get takeSalary(){
        return `You salary is ${this.salary *3}`
    }
};
const incognito = new Employee({name:'Dima', age:'26', salary: 35000});
const allLang = new Programmer( {name:'Ivan', age:'25', salary: 20000, lang:'English'});
console.log(incognito);
console.log(incognito.takeSalary);
console.log(allLang);
console.log(allLang.takeSalary);