const {src, dest, watch} = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const minifyjs = require('gulp-js-minify');
const rename = require("gulp-rename");
const imagemin = require('gulp-imagemin');

const path = {
    src:{
        scss: "./src/scss",
        js: "./src/js",
        img: "./src/img"
    },
    dist:{
        css: './dist/css',
        js: "./dist/",
        img: "./dist/"
    }
};

const dev = function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 8080
    });
    watch('./' + '**/*.html', function (cb) {
        browserSync.reload();
        cb();
    });
    watch(path.src.scss + '**/*.scss', function (cb) {
        build();
        browserSync.reload();
        cb();
    });
    watch(path.src.js + "**/*.js", function (cb) {
         minify_JS();
        browserSync.reload();
        cb();
    });
};
const image = function () {
    return src(path.src.img + "**/*.png")
        .pipe(imagemin())
        .pipe(dest(path.dist.img))
};
const minify_JS = function () {
    return src(path.src.js +'**/*.js')
        .pipe(minifyjs())
        .pipe(rename("/js/scripts.min.js"))
        .pipe(dest(path.dist.js))
};
const cleanFolder = function () {
    return src(['./dist/*', '!dist'],{ read: false })
        .pipe(clean({force: true}));
};


const scss = function () {
    return src(path.src.scss + "/**/*.scss")
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename("/styles.min.css"))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(dest(path.dist.css))
};

const build = function(cb){
    // cleanFolder() - очистить папку дист
    minify_JS();
    image();
    scss();
cb()
};
exports.dev = dev;
exports.build = build;
exports.cleanFolder = cleanFolder;