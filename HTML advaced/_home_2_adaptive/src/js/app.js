const btn = document.querySelector(".top-menu__menu-hide");
const openMenu = document.querySelector(".second");
const closeMenu = document.querySelector(".first");
const menu = document.querySelector(".top-menu");
btn.addEventListener('click', function (e) {
    let currentTarget = e.target;
    menu.classList.add("active");
    openMenu.classList.remove('disable');
    currentTarget.closest('span').classList.add("disable");
     if(openMenu.classList.contains('disable')){
         menu.classList.remove("active");
         closeMenu.classList.remove("disable")
     }
});