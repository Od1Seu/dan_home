jQuery(document).ready(function($){
    $('a[href^="#"]').on('click', handler );
 function handler(e) {
     let anchor = $(this).attr('href');
     $('html, body').animate({
         scrollTop: $(anchor).offset().top
     }, 1200);
     e.preventDefault();
     return false;
 }

    let btn = $('#button_Up');

    $(window).scroll(function() {
        if ($(window).scrollTop() > $(window).height()) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });
    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '1000');
    });

    let btnSlide = $('#slideToggle');

    btnSlide.on('click',function () {
        $('.posts').slideToggle('slow')
    })
});


