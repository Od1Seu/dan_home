const slider = document.querySelector('#slider');
let img = slider.querySelector('img');

// Buttons !!!!


const button_1 = document.createElement('button');
button_1.textContent = 'Возобновить';
button_1.style.cssText = 'width: 84px; height: 25px; color: black; background-color: green; border-radius: 5px; outline: none;';
document.body.prepend(button_1);

const button_2 = document.createElement('button');
button_2.textContent = 'Прекратить';
button_2.style.cssText = 'width: 80px; height: 25px; color: black; background-color: yellow; border-radius: 5px; outline: none; margin: 0px 12px 5px 0';
document.body.prepend(button_2);

const stopBtn = document.body.querySelectorAll('button')[0];
const startBtn = document.body.querySelectorAll('button')[1];

const allImg = ['1.jpg','2.jpg','3.jpg','4.jpg'];
img.src = `img/${allImg[0]}`;
let i = 1;
      function showImg() {
          img.src = `img/${allImg[i]}`;
          i++;
          if(i === allImg.length){
              i = 0;
          }
      }
    let timer = setInterval(showImg, 1000);
      
startBtn.addEventListener('click',startSlide);
stopBtn.addEventListener('click',stopSlide);

    function startSlide() {
        clearTimeout(timer);
        timer = setInterval(showImg, 1000);

    }
    function stopSlide() {
        clearTimeout(timer)
    }
