1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?

1. Перша спрацьоювує лише один раз, через заданий проміжок часу. Друга ж, працює періодично, через вказаний проміжок часу.
2. Теоретично спрацює, якщо немає іншого коду який не завершився. Також, є і вн настройки браузера з приводу затримки.
3. Буде забиватися пам'ять, поки її не почистити.
