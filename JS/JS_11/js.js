const btnWrapper = document.querySelector('.btn-wrapper');
document.addEventListener('keypress',  function (e) {
    for(let key of btnWrapper.children){
        key.style.backgroundColor = '#33333a';
      let textOFBtn = key.textContent.toLowerCase();
      if(e.key.toLowerCase() === textOFBtn){
          key.style.backgroundColor = 'blue';
      }
    }
});