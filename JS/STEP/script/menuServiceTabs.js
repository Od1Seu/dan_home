
const getMenu = document.querySelectorAll('.article-nav-item');
getMenu.forEach(function (item) {
    item.addEventListener('click', function (e) {
        let getIdDataElement = this.getAttribute('data-target-tab'),
            content = document.querySelector('.article-container-tubs[data-target-tab="'+getIdDataElement+'"]'),
            activeMenu = document.querySelector('.article-nav-item.active'),
            activeContent = document.querySelector('.article-container-tubs.active');
        activeMenu.classList.remove('active');
        item.classList.add('active');
        activeContent.classList.remove('active');
        content.classList.add('active');
    });
});