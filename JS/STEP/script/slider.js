$(function(){
    $('.sl').slick({
        appendArrows: false,
        asNavFor: '.sl2',
    });
});
$(function(){
    $('.sl2').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        focusOnSelect: true,
        asNavFor: '.sl',
        prevArrow: '<div class="prev-slide"><img class="prev-slide-icon" src="image/theHam/next.png" alt="arrow"></div>',
        nextArrow: '<div class="next-slide"><img class="next-slide-icon" src="image/theHam/prev.png" alt="arrow"></div>',
    });
});
