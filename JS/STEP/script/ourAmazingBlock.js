const menuAmazingService = function () {
    let tabsGallary = document.querySelectorAll('.section-amazing-nav-item');
    let tabsImages = document.querySelectorAll('.section-amazing-img'),
        tabsIsActive;

    tabsGallary.forEach(item => {
        item.addEventListener('click', selectTabsGallary)
    });

    function selectTabsGallary() {
        tabsGallary.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabsIsActive = this.getAttribute('data-target');
        selectActiveTab(tabsIsActive)
    }

    function selectActiveTab(tabsIsActive) {
        tabsImages.forEach(item => {
            item.classList.contains(tabsIsActive) ?
                item.classList.add('active') :
                item.classList.remove('active');
            if (tabsIsActive === '1-amazing') {
                item.classList.add('active');
            }
        })
    }

    const menuAmazingServiceAdd = function () {
        const addImages = document.querySelector('.section-btn');
        const tabsImagesAdd = document.querySelectorAll('.section-amazing-img-hide');
        const preloader = document.querySelector('.loading');
        addImages.addEventListener('click', function () {
            addImages.remove();
            preloader.classList.add('loader');
            const timer = setTimeout(function () {
                tabsImagesAdd.forEach(item=>{
                    item.classList.add('active');
                    item.classList.add('section-amazing-img');
                    tabsImages = document.querySelectorAll('.section-amazing-img');
                    selectActiveTab(tabsIsActive)
                });
                tabsGallary.forEach(item => {
                    if(item.dataset.target === '1-amazing' && item.classList.contains('active')) {
                        tabsImages.forEach(elem => {
                            elem.classList.add('active')
                        })
                    }
                });
                clearInterval(timer);
                preloader.classList.remove('loader');
            },1000);
        } );
    };
    menuAmazingServiceAdd()
};
menuAmazingService();
