const container = document.body.querySelector('.container');
const button = document.createElement('button');
button.textContent = 'Сменить тему';
button.style.cssText = "height: 39px;\n" +
    "    border-radius: 3px;\n" +
    "    background-color: #f26522;\n" +
    "    outline: none;\n" +
    "    text-shadow: 1px greenyellow";
container.prepend(button);

button.addEventListener('click', changeBackground);
window.addEventListener('DOMContentLoaded', readyToChange);
let loadColor =  document.body.style.backgroundColor;
let flag = 0;

function changeBackground(){
     if(flag === 0){
        localStorage.clear();
         const backGround_1 =  document.body.style.backgroundColor = '#79b027';
        localStorage.setItem('secondBack', backGround_1);
       flag = 1;
    }else{
        localStorage.clear();
        const backGround_2 = document.body.style.backgroundColor = loadColor;
        localStorage.setItem('firstBack', backGround_2);
        flag = 0;
    }
}
function readyToChange() {
    for (let i = 0; i < localStorage.length; i++) {
        if(localStorage.key(i) === "secondBack"){
            document.body.style.backgroundColor = localStorage.getItem('secondBack');
            flag = 1
        }else if(localStorage.key(i) === "firstBack"){
            document.body.style.backgroundColor = localStorage.getItem('firstBack')
        }
    }
}


