 function validate(valid) {
     while (valid === null || valid === '' || isNaN(valid) ) {
         valid = prompt('Add some number', valid);
     }
     return valid
 }

function factorialNumber(n) {
    return ( n === 1 ) ? n : n * factorialNumber(n -1)

}

const enterNumber = validate(
    prompt('Add some number', 1));

let getFactorial = factorialNumber(enterNumber);
console.log(getFactorial);
