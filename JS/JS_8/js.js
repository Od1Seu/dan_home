const div = document.getElementById('div');
const inputPrice = document.getElementById('numPrice');
const span = document.createElement('span');
const btn = document.createElement('button');
const p = document.createElement('p');
    p.textContent = 'Please enter correct price';
    btn.textContent = 'X';

    inputPrice.addEventListener('focus', inputFocus);
    inputPrice.addEventListener('blur', inputBlur);
    btn.addEventListener('click', clickBtn);

    function inputFocus() {
            inputPrice.style.border = '2px solid green';
    }

    function inputBlur() {
        if(inputPrice.value < 0){
            inputPrice.style.border = '3px solid red';
            inputPrice.style.backgroundColor = 'white';
            span.innerHTML = '';
            div.append(p)
        }else {
            inputPrice.style.border = '1px solid black';
            inputPrice.style.backgroundColor = 'green';
            btn.style.marginLeft = '10px';
            span.innerHTML = `Текущая цена: $ ${inputPrice.value}`;
            p.remove();
            document.body.prepend(btn);
            document.body.prepend(span);
        }
    }
    function clickBtn() {
        inputPrice.style.backgroundColor = '';
        span.innerHTML = '';
        inputPrice.value= '';
        btn.remove()

    }
