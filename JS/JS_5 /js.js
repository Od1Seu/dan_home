function createNewUser(){
    const firstName = prompt('Enter u firstName');
    const lastName = prompt('Enter u lastName');
    let birthday = prompt('Enter u date of birthday', 'dd.mm.yyyy');
    const now = new Date(birthday);
    const newUser={
        firstName,
        lastName,
        getLogin (){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getAge(){
            let dateAndMount = birthday.split('');
            const newDate = dateAndMount.slice(3,6) + dateAndMount.slice(0,3) + dateAndMount.slice(6);
            return new Date(newDate.split(',').join(''));
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + now.getFullYear();
        }
    };
    Object.defineProperties(newUser, {
        firstName: {value: firstName, writable: false},
        lastName: {value: lastName, writable: false}
    });

    console.log( newUser.getLogin());
    return newUser
}
let user = createNewUser();
user.getLogin();
console.log(user.getAge());
console.log(user.getPassword());
console.log(`user.getLogin(): ${user.getLogin()}`)