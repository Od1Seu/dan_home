import React from 'react';
import './App.scss'
import AppRoutes from "./Components/routes/AppRoutes";
import Sidebar from "./Components/SideBar/Sidebar";

const App = () => {
  const state = {
    cards: [],
    modalInfo: {
      isOpened: true,
      idCard: null,
      nameCart: ''
    }
  }
  return (
    <>
      <Sidebar/>
      <AppRoutes state={state}/>
    </>
  )
}

export default App;



