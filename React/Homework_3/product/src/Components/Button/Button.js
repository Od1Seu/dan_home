import React from 'react';
import './Button.scss'

const Button = (props) => {
  const {btnHandler, btnText} = props;
  return (
    <div className='btnPosition'>
      <button onClick={btnHandler} className='btnGroup'>{btnText}</button>
    </div>
  );
};

export default Button;