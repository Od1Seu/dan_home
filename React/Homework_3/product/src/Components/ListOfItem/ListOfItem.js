import React, {useState, useEffect} from 'react';
import Cart from "../ItemCart/ItemCart";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import Header from "../Header/Header";

const ListOfItem = () => {
  const [cards, setCards] = useState([]);
  const [modalIsOpened, setModalIsOpened] = useState(true);
  const [modalIdCard, setModalIdCard] = useState(null);
  const [modalNameCart, setModalNameCart] = useState('');

  useEffect(() => {
    fetch('/items.json')
      .then(res => res.json())
      .then((result) => {
        const currentCart = normalize(result.items);
        setCards(currentCart);
      })
  }, []);

  const normalize = (cards) => {
    const valueCart = JSON.parse(localStorage.getItem('addedToCard')) || [];
    const valueFavorite = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];

    return cards.map(cart => {
      cart.isFavourite = valueFavorite.includes(cart.article);
      cart.isAddedToCart = valueCart.includes(cart.article);
      return cart
    })
  };

  const isOpened = (el) => {
    setModalIsOpened(!modalIsOpened);
    setModalIdCard(el.article);
    setModalNameCart(el.name);
  };

  const addToCart = () => {
    const card = cards.map(el => {
      if (el.article === modalIdCard) {
        el.isAddedToCart = !el.isAddedToCart
      }
      return el
    });

    setModalIsOpened(!modalIsOpened);
    setCards(card);
    addToLocal();
  };

  const addToLocal = () => {
    let addedToCards = JSON.parse(localStorage.getItem("addedToCard")) || [];
    addedToCards.includes(modalIdCard) ?
      addedToCards.filter(el => el) :
      addedToCards.push(modalIdCard);
    localStorage.setItem('addedToCard', JSON.stringify(addedToCards))
  };

  const toggleFavorite = (code) => {
    const addToFavorite = cards.map(item => {
      if (code === item.article) {
        item.isFavourite = !item.isFavourite
      }
      return item
    });

    addToLocalFavorite(code);
    setCards(addToFavorite);
  };

  const addToLocalFavorite = (code) => {
    let addedToCardsFavorite = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];
    addedToCardsFavorite = (addedToCardsFavorite.includes(code) ?
      addedToCardsFavorite.filter(el => el !== code) :
      addedToCardsFavorite.concat(code));
    localStorage.setItem('addedToCardFavorite', JSON.stringify(addedToCardsFavorite))
  };

  const isCancel = () => {
    setModalIsOpened(!modalIsOpened);
  };

  return (
    <div>
      <Header/>
      <div className='wrapper'>
        {cards.map((el, index) => (
          <Cart
            card={el}
            key={index}
            name={el.name}
            type={el.type}
            price={el.price}
            url={el.url}
            iconHand={() => toggleFavorite(el.article)}
            isOpened={isOpened.bind(null, el)}
          >
            {!modalIsOpened && <Modal
              isModal={isCancel}
              btnHandler={isCancel}
              modalText={modalNameCart}
              action={{
                btnOk: () => <Button btnHandler={addToCart} btnText={'Ok'}/>,
                btnCancel: () => <Button btnHandler={isCancel} btnText={'Cancel'}/>
              }}
            />}
          </Cart>
        ))}

      </div>
    </div>
  );
};
Cart.defaultProps = {
  name: 'Some Items',
  type: 'Items'
};
Modal.defaultProps = {
  header: 'Do you want to add this items to ItemCart?',
  modalText: 'this is a item of music'
};
Cart.propTypes = {
  card: PropTypes.object,
  name: PropTypes.string,
  type: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  icon: PropTypes.string,
  iconHand: PropTypes.func,
  isOpened: PropTypes.func,
};
Modal.propTypes = {
  isModal: PropTypes.func,
  btnHandler: PropTypes.func,
  header: PropTypes.string,
  modalText: PropTypes.string,
  action: PropTypes.object.isRequired,
};
Button.propTypes = {
  btnHandler: PropTypes.func.isRequired,
  btnText: PropTypes.string
};

export default ListOfItem;