import React, {useState, useEffect} from 'react';
import Cart from "../../ItemCart/ItemCart";

const Favorite = () => {
  const [data, setData] = useState([]);
  const favoriteItem = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];

  useEffect(() => {
    fetch('/items.json')
      .then(res => res.json())
      .then(res => {
        const currentFavorite = normalize(res.items)
        const getFavorite = currentFavorite.filter((item) => favoriteItem.includes(item.article));
        setData(getFavorite)
      })
  }, []);

  const normalize = (favorites) => {
    const valueFavorite = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];
    return favorites.map(cart => {
      cart.isFavourite = valueFavorite.includes(cart.article);
      return cart
    })
  };

  const toggleFavorite = (code) => {
    const addToFavorite = data.map((item) => {
      if (code === item.article) {
        item.isFavourite = !item.isFavourite
      }
      return item
    });
    addToLocalFavorite(code)
    setData(addToFavorite.filter((el) => el.article !== code))
  };

  const addToLocalFavorite = (code) => {
    let addedToCardsFavorite = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];
    addedToCardsFavorite = (addedToCardsFavorite.includes(code) ?
      addedToCardsFavorite.filter(el => el !== code) :
      addedToCardsFavorite.concat(code));
    localStorage.setItem('addedToCardFavorite', JSON.stringify(addedToCardsFavorite))
  };

  return (
    <div className='Card'>
      {data.map((el, index) => (
        <Cart
          card={el}
          key={index}
          name={el.name}
          type={el.type}
          price={el.price}
          url={el.url}
          iconHand={() => toggleFavorite(el.article)}
        />
      ))}
    </div>
  );
};

export default Favorite;