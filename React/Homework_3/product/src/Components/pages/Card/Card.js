import React, {useEffect, useState} from 'react';
import Cart from "../../ItemCart/ItemCart";
import './Card.scss'
import Modal from "../../Modal/Modal";
import Button from "../../Button/Button";


const Card = () => {
  const [data, setData] = useState([]);
  const [isOpened, setIsOpened] = useState(false);
  const cartItem = JSON.parse(localStorage.getItem('addedToCard')) || [];

  useEffect(() => {
    fetch('/items.json')
      .then(res => res.json())
      .then(res => {
        const getCart = res.items.filter((item) => cartItem.includes(item.article))
        setData(getCart)
      })
  }, []);

  const handlerModalCard = () => {
    setIsOpened(!isOpened)
  };

  const removeItem = (e) => {
    const removeElement = data.filter((el) => el.article !== e.article);
    const deleteFromLocal = cartItem.filter((el) => el !== e.article);
    localStorage.removeItem('addedToCard');
    localStorage.setItem('addedToCard', JSON.stringify(deleteFromLocal));
    setData(removeElement);
    setIsOpened(!isOpened)
  };

  return (
    <div className='Card'>
      {data.map((el, index) => (
        <Cart
          status={false}
          card={el}
          key={index}
          name={el.name}
          type={el.type}
          price={el.price}
          url={el.url}
          isOpened={handlerModalCard}
        >
          {isOpened && <Modal
            isModal={handlerModalCard}
            btnHandler={handlerModalCard}
            modalText='Do u want to delete item?'
            action={{
              btnOk: () => <Button btnHandler={removeItem.bind(this, el)} btnText={'Ok'}/>,
              btnCancel: () => <Button btnHandler={handlerModalCard} btnText={'Cancel'}/>
            }}
          />}
        </Cart>
      ))}
    </div>
  );
};

export default Card;