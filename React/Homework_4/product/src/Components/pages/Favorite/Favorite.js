import React from 'react';
import Cart from "../../ItemCart/ItemCart";
import {useDispatch, useSelector} from "react-redux";
import {getItemInCard} from "../../../store/card/action";

const Favorite = () => {
  const card = useSelector(state => state.card.data);
  const isFavorite = useSelector(state => state.isFavourite);

  const favoriteItem = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];
  let getCart = card.filter((item) => favoriteItem.includes(item.article));

  const dispatch = useDispatch();


  const toggleFavorite = (code) => {
    const addToFavorite = getCart.map((item) => {
      if (code === item.article) {
        item.isFavourite = !item.isFavourite
      }
      return item
    });
    addToLocalFavorite(code);
    dispatch(getItemInCard(addToFavorite.filter((el) => el.article !== code)))
  };

  const addToLocalFavorite = (code) => {
    let addedToCardsFavorite = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];
    addedToCardsFavorite = (addedToCardsFavorite.includes(code) ?
      addedToCardsFavorite.filter(el => el !== code) :
      addedToCardsFavorite.concat(code));
    localStorage.setItem('addedToCardFavorite', JSON.stringify(addedToCardsFavorite))
  };

  return (
    <div className='Card'>
      {getCart.map((el, index) => (
        <Cart
          card={el}
          key={index}
          name={el.name}
          type={el.type}
          price={el.price}
          url={el.url}
          iconHand={() => toggleFavorite(el.article)}
        />
      ))}
    </div>
  );
};

export default Favorite;