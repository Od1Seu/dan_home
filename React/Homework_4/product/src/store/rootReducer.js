import {ADD_TO_CARD, ADD_TO_FAVORITE, GET_ITEM_FROM_CARD, LOAD_CARD_REQUEST, LOAD_CARD_SUCCESS} from "./card/type";
import {GET_CARD_ID, GET_CARD_NAME, TOGGLE_MODAL} from "./modal/type";

const initialState = {
  card: {
    data: [],
    isLoading: true
  },
  isFavourite: [],
  modal: {
    isOpening: false,
    idCard: null,
    nameOfCard: ''
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_CARD_REQUEST:
      return {...state, card: {...state.card, isLoading: action.payload}};
    case LOAD_CARD_SUCCESS:
      const normalize = cards => {
        const valueCart = JSON.parse(localStorage.getItem('addedToCard')) || [];
        const valueFavorite = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];
        return cards.map(cart => {
          cart.isFavourite = valueFavorite.includes(cart.article);
          cart.isAddedToCart = valueCart.includes(cart.article);
          return cart
        })
      };
      return {...state, card: {...state.card, data: normalize(action.payload)}};
    case ADD_TO_CARD:
      return {...state, card: {...state.card, data: action.payload}};
    case  ADD_TO_FAVORITE:
      return {...state, card: {...state.card, data: action.payload}};
    case GET_ITEM_FROM_CARD:
      return {...state, isFavourite: action.payload};
    case TOGGLE_MODAL:
      return {...state, modal: {...state.modal, isOpening: !action.payload}};
    case GET_CARD_ID:
      return {...state, modal: {...state.modal, idCard: action.payload}};
    case GET_CARD_NAME:
      return {...state, modal: {...state.modal, nameOfCard: action.payload}};
    default:
      return state
  }
};


export default reducer;