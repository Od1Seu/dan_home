import {ADD_TO_CARD, ADD_TO_FAVORITE, GET_ITEM_FROM_CARD, LOAD_CARD_REQUEST, LOAD_CARD_SUCCESS} from './type'

export const getRequest = () => {
 return {type: LOAD_CARD_REQUEST, payload: true}
};

export const getCardSuccess = data => {
  return {type: LOAD_CARD_SUCCESS, payload: data }
};

export const addToCard = data =>{
  return { type: ADD_TO_CARD, payload: data}
};

export const addToFavorites = data =>{
  return {type: ADD_TO_FAVORITE, payload: data}
};

export const getItemInCard = data =>{
  return {type: GET_ITEM_FROM_CARD, payload: data}
};