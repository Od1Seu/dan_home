import axios from "axios";
import { getRequest, getCardSuccess } from './action'

const getCard = ()=>(dispatch)=>{
  dispatch(getRequest());
    axios('/items.json').then(res=>{
  dispatch(getCardSuccess(res.data.items));
    })
};
export default getCard;
