import React, {useEffect} from 'react';
import './App.scss'
import AppRoutes from "./Components/routes/AppRoutes";
import Sidebar from "./Components/SideBar/Sidebar";
import {useDispatch} from "react-redux";
import getCard from './store/card/operation'


const App = () => {

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCard());
  }, []);

  return (
    <>
      <Sidebar/>
      <AppRoutes />
    </>
  )
};

export default App;



