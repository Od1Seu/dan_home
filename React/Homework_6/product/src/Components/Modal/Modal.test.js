import {render, screen} from "@testing-library/react";
import Modal from "./Modal";
import React from "react";
import '@testing-library/jest-dom'
import userEvent from "@testing-library/user-event";

const action = {
  btnOk: () => <div/>,
  btnCancel: () => <div/>
};

describe('Testing Modal.js',()=>{

  test('Smoke test modal',()=>{
    render(<Modal action={action}/>)
  });

  test('Modal header get a header props', ()=>{
    const headerName = 'title';
    render(<Modal action={action} header={headerName}/>);
    const header = screen.getByTestId('modal-header');
    expect(header).toBeInTheDocument();
    expect(header.textContent).toBe(headerName);
  });

  test('Click to span and close modal window', ()=>{
    const toggleModal = jest.fn();
    render(<Modal action={action} isModal={toggleModal}/>);
    expect(toggleModal).not.toHaveBeenCalled();
    userEvent.click(screen.getByTestId('close-modal'));
    expect(toggleModal).toHaveBeenCalled();

  });

  test('Modal body(props) render information about items', ()=>{
    const itemsDescribe = 'information about items';
    render(<Modal action={action} modalText={itemsDescribe}/>);
    const modalText = screen.getByTestId('describe-item');
    expect(modalText).toBeInTheDocument();
    expect(modalText.textContent).toBe(itemsDescribe);
  });

});