import React,{useEffect} from 'react';
import Cart from "../ItemCart/ItemCart";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import PropTypes from 'prop-types';
import Header from "../Header/Header";
import {useDispatch, useSelector} from "react-redux";
import { addToCard, addToFavorites} from "../../store/card/action";
import {getCardId, getCardName, toggleModal} from "../../store/modal/action";
import getCard from "../../store/card/operation";

const ListOfItem = () => {
  const dispatch = useDispatch();
  const cards = useSelector(state => state.card.data);
  const modal = useSelector(state => state.modal);
  const {isOpening, idCard, nameOfCard} = modal;
  useEffect(() => {
    dispatch(getCard());
  }, [dispatch]);


  const isOpened = (el) => {
    dispatch(toggleModal(isOpening));
    dispatch(getCardId(el.article));
    dispatch(getCardName(el.name));
  };

  const addToCart = () => {
    const card = cards.map(el => {
      if (el.article === idCard) {
        el.isAddedToCart = !el.isAddedToCart;
      }
      return el
    });
    dispatch(toggleModal(isOpening));
    dispatch(addToCard(card));
    addToLocal();
  };

  const addToLocal = () => {
    let addedToCards = JSON.parse(localStorage.getItem("addedToCard")) || [];
    addedToCards.includes(idCard) ?
      addedToCards.filter(el => el) :
      addedToCards.push(idCard);
    localStorage.setItem('addedToCard', JSON.stringify(addedToCards))
  };

  const toggleFavorite = (code) => {
    const addToFavorite = cards.map(item => {
      if (code === item.article) {
        item.isFavourite = !item.isFavourite;
      }
      return item
    });

    addToLocalFavorite(code);
    dispatch(addToFavorites(addToFavorite));
  };

  const addToLocalFavorite = (code) => {
    let addedToCardsFavorite = JSON.parse(localStorage.getItem('addedToCardFavorite')) || [];
    addedToCardsFavorite = (addedToCardsFavorite.includes(code) ?
      addedToCardsFavorite.filter(el => el !== code) :
      addedToCardsFavorite.concat(code));
    localStorage.setItem('addedToCardFavorite', JSON.stringify(addedToCardsFavorite))
  };

  const isCancel = () => {
    dispatch(toggleModal(isOpening));
  };

  return (
    <div>
      <Header/>
      <div className='wrapper'>
        {cards.map((el, index) => (
          <Cart
            card={el}
            key={index}
            name={el.name}
            type={el.type}
            price={el.price}
            url={el.url}
            iconHand={() => toggleFavorite(el.article)}
            isOpened={isOpened.bind(null, el)}
          >
            {isOpening && <Modal
              isModal={isCancel}
              btnHandler={isCancel}
              modalText={nameOfCard}
              action={{
                btnOk: () => <Button btnHandler={addToCart} btnText={'Ok'}/>,
                btnCancel: () => <Button btnHandler={isCancel} btnText={'Cancel'}/>
              }}
            />}
          </Cart>
        ))}

      </div>
    </div>
  );
};
Cart.defaultProps = {
  name: 'Some Items',
  type: 'Items'
};
Modal.defaultProps = {
  header: 'Do you want to add this items to ItemCart?',
  modalText: 'this is a item of music'
};
Cart.propTypes = {
  card: PropTypes.object,
  name: PropTypes.string,
  type: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  icon: PropTypes.string,
  iconHand: PropTypes.func,
  isOpened: PropTypes.func,
};
Modal.propTypes = {
  isModal: PropTypes.func,
  btnHandler: PropTypes.func,
  header: PropTypes.string,
  modalText: PropTypes.string,
  action: PropTypes.object.isRequired,
};
Button.propTypes = {
  btnHandler: PropTypes.func.isRequired,
  btnText: PropTypes.string
};

export default ListOfItem;