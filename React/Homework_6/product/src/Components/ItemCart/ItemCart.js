import React from 'react';
import './ItemCart.scss'
import {star} from '../../theme/icons/star'

const Cart = (props) => {
  const {name, type, price, url, isOpened, children, iconHand, card} = props;
  return (
    <div>
      <div className='itemsRender'>
        <div className="items">
          <img src={url} alt="" className="items-image"/>
        </div>
        <div className="items-header">
          <h4 className="items-header-name">{name}</h4>
        </div>
        <div className="items-body">
          <p className="items-body-text">{type}</p>
          {!!iconHand && <span className='items-body-icon' onClick={iconHand}>
                 {star(card.isFavourite ? 'blue' : 'grey')}
            </span>}
        </div>
        <div className="items-footer">
          <p className="items-footer-price">{price}</p>
          <button data-testid='openModal' className="items-footer-btn"  onClick={isOpened}> {!!iconHand ? 'Add to Cart' : 'Remove'}</button>
        </div>
      </div>
      {children}
    </div>
  );
};

export default Cart;