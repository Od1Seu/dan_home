import {render} from "@testing-library/react";
import ItemCart from './ItemCart'
import userEvent from "@testing-library/user-event";

describe('Testing ItemsCard.js', ()=>{
  test('Smoke test ItemsCard', ()=>{
      render(<ItemCart/>)
  });
  test('Button should open a modal window ', ()=> {
      const modal = jest.fn();

      const {getByTestId}= render(<ItemCart isOpened={modal} />);
          expect(modal).not.toHaveBeenCalled();
          userEvent.click(getByTestId('openModal'));
          expect(modal).toHaveBeenCalled();
          expect(modal).not.toHaveBeenCalledWith()
  });
});
