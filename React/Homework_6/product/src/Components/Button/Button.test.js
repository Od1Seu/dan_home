import {render,screen} from "@testing-library/react";
import Button from "./Button";
import userEvent from "@testing-library/user-event";
import '@testing-library/jest-dom'

describe('Testing button.js', ()=>{
  test('Smok button ', ()=>{
    render(<Button/>)
  });

  test('Button has been open on click', ()=>{
    const btnHandler = jest.fn();
    render(<Button btnHandler={btnHandler}/>);
    const btn = screen.getByTestId('button');
    expect(btnHandler).not.toHaveBeenCalled();
    expect(btn).toBeInTheDocument();
    userEvent.click(btn);
    expect(btnHandler).toHaveBeenCalled()
  });

  test('Button props has equal text content in button', ()=>{
    const nameButton = 'button_Name';
    render(<Button nameButton={nameButton}/>);
    const btn = screen.getByTestId('button');
      btn.textContent = nameButton;
    expect(btn).toBeInTheDocument();
    expect(btn.textContent).toBe(nameButton)

  })

});