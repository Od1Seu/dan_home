import React from 'react';
import { NavLink} from "react-router-dom";
import logo from  './img/icon.png'
import './Sidebar.scss'

const Sidebar = () => {
  return (
    <>
      <div data-testid='sidebar' className='sidebar'>
        <NavLink  data-testid='home-link' exact to='/' activeClassName='sidebar-is-active'>
          <h1   className="sidebar-title">Metallica</h1>
        </NavLink>
        <div className="sidebar-link-block">
          <div className="sidebar-favorite">
            <NavLink data-testid='favorite' to='/favorite' activeClassName='sidebar-is-active'>
              <p className="sidebar-favorite-link">
                Favorites
              </p>
            </NavLink>
          </div>
          <NavLink data-testid='cart' to='/cart' activeClassName='sidebar-is-active'>
            <div className="sidebar-card">
              <img src={logo} alt="icon" className="sidebar-card-icon"/>
              <p className="sidebar-card-text">Cart </p>
            </div>
          </NavLink>
        </div>
      </div>
    </>
  );
};

export default Sidebar;