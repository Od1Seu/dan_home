import {GET_CARD_ID, GET_CARD_NAME, TOGGLE_MODAL} from "./type";

export const toggleModal = (data) => {
  return {type: TOGGLE_MODAL, payload: data}
};

export const getCardId = (data) => {
  return {type: GET_CARD_ID, payload: data}
};

export const getCardName = (data) => {
  return {type: GET_CARD_NAME, payload: data}
};