import React from 'react';
import Cart from "../../ItemCart/ItemCart";
import './Card.scss'
import Modal from "../../Modal/Modal";
import Button from "../../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {toggleModal} from "../../../store/modal/action";
import FormCard from "../../FormCard/FormCard";

const Card = () => {
  const dispatch = useDispatch();
  const isOpened = useSelector(state => state.modal.isOpening);
   const cards = useSelector(state => state.card.data);
   const cartItem = JSON.parse(localStorage.getItem('addedToCard')) || [];
   let getCart = cards.filter((item) => cartItem.includes(item.article));

  const handlerModalCard = () => {
    dispatch(toggleModal(isOpened))
  };
  const removeItem = (e) => {
    const deleteFromLocal = cartItem.filter((el) => el !== e.article);
    localStorage.removeItem('addedToCard');
    localStorage.setItem('addedToCard', JSON.stringify(deleteFromLocal));
    dispatch(toggleModal(isOpened))
  };

  return (
    <div className='Card'>
      <div className="Card-right">
        {getCart.map((el, index) => (
          <Cart
            status={false}
            card={el}
            key={index}
            name={el.name}
            type={el.type}
            price={el.price}
            url={el.url}
            isOpened={handlerModalCard}
          >
            {isOpened && <Modal
              isModal={handlerModalCard}
              btnHandler={handlerModalCard}
              modalText='Do u want to delete item?'
              action={{
                btnOk: () => <Button btnHandler={removeItem.bind(this, el)} btnText={'Ok'}/>,
                btnCancel: () => <Button btnHandler={handlerModalCard} btnText={'Cancel'}/>
              }}
            />}
          </Cart>
        ))}
      </div>
      <div className="Card-left">
        {getCart.length === 0 ? null :  <FormCard />}
      </div>
    </div>
  );
};


export default Card