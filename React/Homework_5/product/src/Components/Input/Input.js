import React from 'react';
import {useField} from "formik";

const Input = (props) => {
  const {type, label, name} = props
  const [field, meta] = useField(name);
  return (
    <div>
      <div>
        <label>
          {label}
          <input type={type}  {...field} />
        </label>
      </div>
      {meta.error && meta.touched && (
        <span style={{color: 'red'}}>{meta.error}</span>
      )}
    </div>
  );
};


export default Input;