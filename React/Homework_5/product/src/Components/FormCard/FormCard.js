import React from 'react';
import './formCard.scss'
import * as yup from 'yup';
import {Formik, Form} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {removeEl} from "../../store/card/action";
import Input from "../Input/Input";
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;


const FormCard = () => {
   const card = useSelector(state => state.card.data);
   const cartItem = JSON.parse(localStorage.getItem('addedToCard')) || [];
   const getCart = card.filter((item) => cartItem.includes(item.article));
   const dispatch = useDispatch();
  const validateForm = yup.object().shape({
    login: yup.string()
      .min(3, 'Too short')
      .max(15, 'Too Long')
      .required('Required'),
    surName: yup.string()
      .min(3, 'Too short')
      .max(15, 'Too Long')
      .required('Required'),
    age: yup.number()
      .required('Required'),
    address: yup.string()
      .min(3, 'Too short')
      .max(25, 'Too Long')
      .required('Required'),
    mobile: yup.string()
      .matches(phoneRegExp, 'Invalid phone')
      .required("Phone is required")
   });

  const submitForm = (value) =>{
    console.group();
    console.log("Information from client:");
    console.log(value);
    console.log("Information about purchases:");
   {getCart.map((el)=>{
      console.log(`Name of product is:  ${el.name}. Price is :  ${el.price}`)
     return el
    })}
    console.groupEnd();
     dispatch(removeEl());
    localStorage.removeItem('addedToCard')
  };

  return (
    <div className='form'>
      <Formik
      initialValues={{
          login:'',
          surName:'',
          age:'',
          address:'',
          mobile:'',
        }}
      onSubmit={async (values, { resetForm }) => {
        await submitForm(values);
        resetForm()
      }}
      validationSchema={validateForm}
      >
        {() => {
          return(
            <Form>
              <Input name='login' type='text' label='You Name:'/>
              <Input name='surName' type='text' label='Second Name:'/>
              <Input name='age' type='number' label='Age:'/>
              <Input name='address' type='text' label='Address'/>
              <Input name='mobile' type='number' label='You mobile phone:'/>
              <button type='submit'>Checkout</button>
            </Form>
          )
        }}
      </Formik>
    </div>
  );
};

export default FormCard;