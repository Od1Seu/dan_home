import React from 'react';
import './Modal.scss'

const Modal =(props)=> {
    const {modalText, header, action, isModal} = props;
    return (
      <>
        {isModal && <div className='overlay' onClick={isModal}/>}
        <div className='modal-wrapper'>
          <div className="modal-header">
            <h1 className="modal-header-title">{header}</h1>
            <span onClick={isModal} className="modal-header-close">{'X'}</span>
          </div>
          <div className="modal-content">
            <div className="modal-body">
              <p className="modal-body-text">{modalText}</p>
            </div>
          </div>
          <div className="modal-footer">
            {action.btnOk()}
            {action.btnCancel()}
          </div>
        </div>
      </>
    );
}

export default Modal;