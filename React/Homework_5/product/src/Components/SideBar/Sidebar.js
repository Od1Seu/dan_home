import React from 'react';
import { NavLink} from "react-router-dom";
import logo from  './img/icon.png'
import './Sidebar.scss'

const Sidebar = () => {
  return (
    <>
      <div className='sidebar'>
        <NavLink exact to='/' activeClassName='sidebar-is-active'>
          <h1 className="sidebar-title">Metallica</h1>
        </NavLink>
        <div className="sidebar-link-block">
          <div className="sidebar-favorite">
            <NavLink to='/favorite' activeClassName='sidebar-is-active'>
              <p className="sidebar-favorite-link">
                Favorites
              </p>
            </NavLink>
          </div>
          <NavLink to='/cart' activeClassName='sidebar-is-active'>
            <div className="sidebar-card">
              <img src={logo} alt="icon" className="sidebar-card-icon"/>
              <p className="sidebar-card-text">Cart </p>
            </div>
          </NavLink>
        </div>
      </div>
    </>
  );
};

export default Sidebar;