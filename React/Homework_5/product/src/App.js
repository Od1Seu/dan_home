import React, {useEffect} from 'react';
import './App.scss'
import AppRoutes from "./Components/routes/AppRoutes";
import Sidebar from "./Components/SideBar/Sidebar";
import getCard from "./store/card/operation";
import {useDispatch} from "react-redux";


const App = () => {

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCard());
  }, [dispatch]);

  return (
    <>
      <Sidebar/>
      <AppRoutes />

    </>
  )
};

export default App;



