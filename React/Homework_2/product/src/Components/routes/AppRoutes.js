import React from 'react';
import {Route, Switch} from "react-router";
import ListOfItem from "../ListOfItem/ListOfItem";
import Card from "../pages/Card/Card";
import Favorite from '../pages/Favorite/Favorite'
import Page404 from "../pages/Page404/Page404";

const AppRoutes = (props) => {
  return (
    <div>
      <Switch>
        <Route exact path='/' component={ListOfItem}/>
        <Route exact path='/cart' render={()=><Card state={props}/> }/>
        <Route exact path='/favorite' component={Favorite}/>
        <Route exact path='*' component={Page404}/>
      </Switch>
    </div>
  );
};

export default AppRoutes;