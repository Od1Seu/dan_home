import React from 'react';
import './Header.scss'
import headerImg from './img/headerBg.jpg'

const Header = () => {
  return (
    <>
      <div>
        <img className='header-background' src={headerImg} alt="MetA"/>
      </div>
    </>
  );
}

export default Header;