import React, {Component} from 'react';
import './App.scss'
import Modal from "./Components/Modal/Modal";
import Button from "./Components/Button/Button";

const allInformation = {
    header:{
        first: `Hello from Modal`,
        second: `Hello from Second Modal`
    },
    modalText: {
        first:  `Lorem ipsum dolor sit amet.`,
        second: `Lorem ipsum dolor sit amet. From Second Modal`,
    },
    btnChange:{
        firstBtn: 'green',
        secondBtn: 'Blue',
        textFirst: 'OpenFirstModal',
        textSecond: 'OpenSecondModal',
        textFirstInside: 'Ok',
        textSecondInside: 'Cancel',
        textFirstInsideColor:'yellow',
        textSecondInsideColor:'white',
        secondModalText1: "Accept",
        secondModalText2: "Wrong",
        secondModalColor1:'grey',
        secondModalColor2:'green',

    }
};

class App extends Component {
    state ={
        isOpenFirst: false,
        isOpenSecond: false,
        closeButton: true,
    };
    firstBtnHandler =()=>{
        this.setState({
            isOpenFirst: !this.state.isOpenFirst
        })
};
    handleClose =()=>{
        this.setState({
            isOpenFirst: !this.state.isOpenFirst
        })
    };
    handleCloseSecond =()=>{
        this.setState({
            isOpenSecond: !this.state.isOpenSecond
        })
    };
    secondBtnHandler =()=>{
        this.setState({
            isOpenSecond: !this.state.isOpenSecond
        })
    };

  render() {
      const {isOpenFirst,isOpenSecond,closeButton} = this.state;
        return (

            <div className='App'>
                <div>
                    <Button btnBackgroundColor={allInformation.btnChange.firstBtn} btnText={allInformation.btnChange.textFirst} BtnHandler={this.firstBtnHandler}/>
                    <Button btnBackgroundColor={allInformation.btnChange.secondBtn} btnText={allInformation.btnChange.textSecond} BtnHandler={this.secondBtnHandler} />
                </div>
                {isOpenFirst &&
                    < Modal
                        isOpenFirst={this.handleClose}
                        BtnHandler={this.firstBtnHandler}
                        header={allInformation.header.first}
                        modalText={allInformation.modalText.first}
                        closeButton={closeButton}
                        action={{
                            btnOk: ()=> <Button btnBackgroundColor={allInformation.btnChange.textFirstInsideColor} btnText={allInformation.btnChange.textFirstInside} />,
                            btnCancel: ()=><Button BtnHandler={this.firstBtnHandler}  btnBackgroundColor={allInformation.btnChange.textSecondInsideColor} btnText={allInformation.btnChange.textSecondInside}  />
                        }}/>
                }
                {isOpenSecond &&
                    < Modal
                        isOpenSecond={this.handleCloseSecond}
                        BtnHandler={this.secondBtnHandler}
                        header={allInformation.header.second}
                        modalText={allInformation.modalText.second}
                        closeButton={closeButton}
                        action={{
                            btnOk: ()=> <Button
                                btnBackgroundColor={allInformation.btnChange.secondModalColor1}
                                btnText={allInformation.btnChange.secondModalText1} />,
                            btnCancel: ()=><Button
                                BtnHandler={this.secondBtnHandler}
                                btnBackgroundColor={allInformation.btnChange.secondModalColor2}
                                btnText={allInformation.btnChange.secondModalText2} />
                        }}/>
                }
            </div>
        );
  }
}

export default App;
