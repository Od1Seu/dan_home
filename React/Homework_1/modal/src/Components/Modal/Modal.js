import React, {Component} from 'react';
import './Modal.scss'

class Modal extends Component {
    render() {
        const {modalText, header,closeButton, action,BtnHandler, isOpenFirst, isOpenSecond} = this.props;
            return (
                <>
                    {isOpenFirst &&  <div className='overlay'  onClick={isOpenFirst}/>}
                    {isOpenSecond &&  <div className='overlay'  onClick={isOpenSecond}/>}
                    <div className='modal-wrapper'>
                        <div className="modal-header">
                            <h1 className="modal-header-title">{header}</h1>
                            <span onClick={BtnHandler} className="modal-header-close">{closeButton && 'X'}</span>
                        </div>
                        <div className="modal-content">
                            <div className="modal-body">
                                <p className="modal-body-text">{modalText}</p>
                            </div>
                        </div>
                        <div className="modal-footer">
                            {action.btnOk()}
                            {action.btnCancel()}
                        </div>
                    </div>
                </>
        );
    }
}

export default Modal;