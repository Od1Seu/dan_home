import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    render() {
        const {btnBackgroundColor, btnText, BtnHandler} = this.props;
        return (
            <div className='btnPosition'>
                <button onClick={BtnHandler} style={{background: btnBackgroundColor}} className='btnGroup'>{btnText} </button>
            </div>
        );
    }
}

export default Button;