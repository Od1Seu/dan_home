package home.Enum_Collection;

public enum DayOfWeek {
    SUNDAY ("Воскресенье"),
    MONDAY ("Понедельник"),
    TUESDAY ("Вторник"),
    WEDNESDAY ("Среда"),
    THURSDAY ("Четверг"),
    FRIDAY ("Пятница"),
    SATURDAY ("Суббота");

    private String name;
    DayOfWeek(String title) {
        this.name = title;
    }
    public String getTitle() {
        return name;
    }
    @Override
    public String toString() {
        return "DayOfWeek{" +
                "name='" + name + '\'' +
                '}';
    }
}
