package home.Inherit_Pets;

import home.Pet;
import home.Enum_Collection.Species;

public class Dog extends Pet implements Pet.foul {
    private Species species;
    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        species = species.DOG;

    }

    public Species getSpecies() {
        return species;
    }

    public void respond() {
        System.out.println("gav-gav-gav");
    }

    public void foul(){
      System.out.println("I am dirty dog");
    }
}
