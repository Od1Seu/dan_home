package home.Inherit_Pets;

import home.Pet;
import home.Enum_Collection.Species;

public class DomesticCat extends Pet implements Pet.foul{
    private Species species;
    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        species = species.DomesticCat;
    }

    public Species getSpecies() {
        return species;
    }

    @Override
   public void respond() {
        System.out.println("meow-meow-meow");
    }
    public void foul(){
        System.out.println("I am dirty cat");
    }
}
