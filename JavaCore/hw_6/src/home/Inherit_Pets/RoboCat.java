package home.Inherit_Pets;

import home.Pet;
import home.Enum_Collection.Species;

public class RoboCat extends Pet {
    private Species species;
    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        species = species.RoboCat;
    }

    public Species getSpecies() {
        return species;
    }
    @Override
   public void respond() {
        System.out.println("robo-meow-robo-weow");
    }
}
