package home.Inherit_Pets;

import home.Pet;
import home.Enum_Collection.Species;

public class Fish extends Pet {
    private Species species;
    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        species = species.Fish;
    }

    public Species getSpecies() {
        return species;
    }

    @Override
   public void respond() {
        System.out.println("fish-fish-fish");
    }
}

