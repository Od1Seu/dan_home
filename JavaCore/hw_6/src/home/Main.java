package home;

import home.Inherit_Pets.Dog;
import home.Inherit_Pets.DomesticCat;
import home.Inherits_Human.Man;

public class Main {
    public static void main(String[] args) {

        Dog dogJohn = new Dog("alan",22,22,new String[]{"ds","dsa"});
        dogJohn.eat();
        dogJohn.respond();
        dogJohn.foul();
        System.out.println("SPECIES_____" + dogJohn.getSpecies());
        DomesticCat cat = new DomesticCat("alan2",33,33,new String[]{"ds","dsa"});
        cat.eat();
        cat.respond();
        cat.foul();
        System.out.println("SPECIES_____" +cat.getSpecies());

        Man man = new Man("Hil", "Hillon",1992);
        man.greetPet();
        man.getCar();
    }
}
