package home.Inherits_Human;

import home.Human;

public final class Women extends Human {
    private Women(String name, String surname, int year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet() {
        System.out.println("hello from women pet");
    }

    public void makeup(){
        System.out.println("I always doing makeup to improve my facial skin :(");
    }
}
