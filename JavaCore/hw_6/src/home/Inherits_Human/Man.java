package home.Inherits_Human;

import home.Human;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    @Override
    public void greetPet() {
        System.out.println("hello from men pet");
    }

    public void getCar(){
        System.out.println("I bought car, yesterday");
    }
}
