package home;

import home.Enum_Collection.DayOfWeek;
import home.Inherit_Pets.Dog;

import java.util.Arrays;

public class Human {
    private DayOfWeek dayOfWeek;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Dog doggi = new Dog("alan",22,22,new String[]{"ds","dsa"});
    private Family family;
    private String[][] schedule;

    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name){
        this.name = name;
    }
    public Human(String name, String surname, int year, int iq, Family family){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.family = family;
    }
    public Human(String name, String surname, int year, Family family, int iq,String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
        this.iq = iq;
        this.schedule = schedule;
    }
    public void greetPet (){
        doggi.getNickname();
    }
//    public String describePet (){
//        return ("У меня есть " + dog + " ему " + dog.getAge() + " лет, " + "он"
//                +  (dog.getTrickLevel() > 50 ? "очень хитрый " : "почти не хитрый"));
//    };

    @Override
    public String toString() {
        return "Human{" +
                " name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
}
