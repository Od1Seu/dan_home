package Test;

import home.Family;
import home.Human;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
   public void when_we_getChildrenAId_deleteChild() {
        Family family = new Family();
        family.addChild(new Human("BBBB", "BBB", 2010));
        family.addChild(new Human("aaa", "aaa", 2010));
        boolean result =  family.deleteChild(1);
        boolean expected = true;
        System.out.println(result == expected);
        System.out.println(family.countFamily() == 3); // necessarily will be mather and father = 2 and 2 children(1deleted) =3;
    }
    @Test
    public void when_we_getWrongId_they_dont_deleteChild_and_returnFalse() {
        Family family = new Family();
        family.addChild(new Human("aaa", "aaa", 2010));
        boolean expected = false;
        try {
             boolean result =  family.deleteChild(1);
            System.out.println(result == true);
        }
        catch(Exception e) {
            System.out.println(expected);
            System.out.println(family);
        }
    }
    @Test
    public void when_passOneChildren_Object_should_increaseByOne() {
            Family family = new Family();
            System.out.println(Arrays.toString(family.getChildren())); //empty or null
            family.addChild(new Human("aaa", "aaa", 2010));
            System.out.println(Arrays.toString(family.getChildren())); //return our children;
            int expected = family.getChildren().length; // length of added children
            System.out.println(expected == family.getChildren().length);
    }
    @Test
    public void when_we_will_use_a_methodCountFamily_should_return_count_of_all_family() {
        Family family = new Family(); // mother and father default + array children have default length 1;
        int expected = 2;
        family.addChild(new Human("aaa", "aaa", 2010));
        family.addChild(new Human("aaa", "aaa", 2010));
        family.addChild(new Human("aaa", "aaa", 2010));
        System.out.println(family.countFamily() == family.getChildren().length + expected); //2 +3 = 5;
        family.deleteChild(2);
        family.deleteChild(1);
        family.deleteChild(0);
        System.out.println(family.countFamily() == expected); //2 == 2


    }
}