package Test;

import home.Human;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
class HumanTest {

    @org.junit.jupiter.api.Test
    public void should_return_PatternString_for_Object_which_was_Overriding() {
            Human human = new Human("Yesterday");
            String expected = "Human{" +
                    " name='" +human.getName() + '\'' +
                    ", surname='" + human.getSurname() + '\'' +
                    ", year=" + human.getYear() +
                    ", iq=" + human.getIq() +
                    ", schedule=" + Arrays.deepToString(human.getSchedule()) +
                    '}';
        System.out.println(expected.equals(human.toString()));
        }
    }