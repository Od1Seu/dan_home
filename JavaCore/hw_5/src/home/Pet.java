package home;

import java.util.Arrays;

public class Pet {
   public enum Species{
        CAT,
        DOG,
        RABBIT,
        FISH,
        HAMSTER
    };
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet (){};

    public Pet(Species species, String nickname,int age, int trickLevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Species species, String nickname){
        this.species = species;
        this.nickname = nickname;
    }

    public void eat (){
        System.out.println("Я кушаю!");
    }
    public void respond (){
        System.out.println("Привет, хозяин. Я - "+ this.nickname + ". Я соскучился!");
    }
    public void foul (){
        System.out.println("Нужно хорошо замести следы...");
    }

        @Override
    public String toString() {
        return "Dog{" +
                " nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public void setSpecies(Species species) {
        this.species = species;
    }
    public Species getSpecies() {
        return species;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public String getNickname() {
        return nickname;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public String[] getHabits() {
        return habits;
    }
}
