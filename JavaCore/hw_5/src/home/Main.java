package home;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Pet dogJohn = new Pet(Pet.Species.HAMSTER, "JHON", 18, 80, new String[]{"sleeping,running"});
        dogJohn.eat();
        dogJohn.respond();
        dogJohn.foul();
        System.out.println(dogJohn.toString());

        Human human = new Human("Din", "DinDon", 1993, 88,
                new Family(
                        new Human("Mila", "Milosovich", 1994),
                        new Human("Aron", "Arrr", 1992),
                        new Pet(Pet.Species.HAMSTER, "Mia", 2, 99, new String[]{"sleeping,running"})));
        System.out.println(human.greetPet());
        System.out.println(human.describePet());
        System.out.println(human);

        Human newHuman = new Human("Mike","Mikky",1992, new Family(
                new Human("Mila", "Milosovich", 1994),
                new Human("Aron", "Arrr", 1992),
                new Pet(Pet.Species.HAMSTER, "Mia", 2, 99, new String[]{"sleeping,running"})),
                88,new String[][]{{Human.DayOfWeek.FRIDAY.getTitle()},{"work"}});
        System.out.println(newHuman);

        Family newFamily =  new Family(
                new Human("Mila","Milosovich", 1994),
                new Human("Aron","Arrr", 1992),
                new Pet(Pet.Species.HAMSTER, "Mia", 2, 99, new String[]{"sleeping,running"}));
        System.out.println(newFamily.addChild(new Human("Arr", "Arrr", 2010)));
        System.out.println(newFamily.addChild(new Human("ddsArr", "dsArrr", 2010)));
        System.out.println(newFamily.toString());
        System.out.println( newFamily.deleteChild(1));
        System.out.println(newFamily.toString());
        System.out.println(newFamily.countFamily());

        Family oldFamily =  new Family(
                new Human("Lala","Lalala", 1990),
                new Human("Ar","Aron", 1990),
                new Pet(Pet.Species.HAMSTER, "Miaaa", 2, 99, new String[]{"sleeping,running"}));
        System.out.println(oldFamily.toString());
    }
}
