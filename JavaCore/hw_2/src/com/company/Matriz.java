package com.company;

import java.util.Random;
import java.util.Scanner;

public class Matriz {
    static Random rnd = new Random();
    static Scanner sc = new Scanner(System.in);
    public static String userHit =  "|x|";
    public static String userMiss =  "|*|";

    public static void main(String[] args) {
        String[][] matrix = new String[6][6];
        int rndLine = rnd.nextInt(5) + 1;
        int rndColumn = rnd.nextInt(5) + 1;
        System.out.println(rndLine + " --- " + rndColumn);
        System.out.println("All set. Get ready to rumble!");

        int userNumberLine;
        int userNumberColumn;
        String enteredLine = "Enter number on the line from 1 to 5";
        String enteredColumn = "Enter number on the COLUMN from 1 to 5";
        System.out.println(enteredLine);

        while (true) {
            if (sc.hasNextInt()) {
                userNumberLine = sc.nextInt();
                if (!(Integer.valueOf(userNumberLine) < 1) && !(Integer.valueOf(userNumberLine) > 5)) {
                    System.out.println("Enter number!! on the COLUMN from 1 to 5");
                    if (sc.hasNextInt()) {
                        userNumberColumn = sc.nextInt();
                        if (!(Integer.valueOf(userNumberColumn) < 1) && !(Integer.valueOf(userNumberColumn) > 5)) {
                            if (rndLine == Integer.valueOf(userNumberLine) && rndColumn == Integer.valueOf(userNumberColumn)) {
                                System.out.println("You have won!");
                                matrix[Integer.valueOf(userNumberLine)][Integer.valueOf(userNumberColumn)] = userHit;
                                getMatrix(matrix);
                                return;
                            } else {
                                matrix[Integer.valueOf(userNumberLine)][Integer.valueOf(userNumberColumn)] = userMiss;
                                getMatrix(matrix);
                                System.out.println(enteredLine);
                            }
                        } else{
                            System.out.println(enteredColumn);
                        }
                    } else{
                        System.out.println(enteredColumn);
                        sc.next();
                    }
                } else {
                    System.out.println(enteredLine);
                }
            } else {
                System.out.println(enteredLine);
                sc.next();
            }
        }
    };

    public static void getMatrix(String[][] matrix ){

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                    if(matrix[i][j] == userMiss){
                        setGoal(matrix,i,j,userMiss);
                     }else if(matrix[i][j] == userHit){
                        setGoal(matrix,i,j,userHit);
                }else {
                        setGoal(matrix,i,j, "|-|");
                    };
            };
            System.out.println();
        };
    };

    public static  void  setGoal(String[][] matrix, int i, int j, String field){
        matrix[i][j] = field;
        matrix[0][j] = "|" + j + "|";
        matrix[i][0] = String.valueOf(i);
        System.out.print(matrix[i][j] + "\t");

    };
};