import java.util.*;

public class Array {

    static Random randomizer = new Random();
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int random = randomizer.nextInt(101);
        int [] allNumber = new int[100];

        System.out.println("Enter you name");
        String name = sc.nextLine();
        System.out.println("Let the game begin!, Enter you number");

        for (int i = 2; i > 0; i++) {
            int userNumber = sc.nextInt();
            allNumber[i] = userNumber;
            if(userNumber < random) System.out.println("Your number is too small. Please, try");
            else if(userNumber > random) System.out.println("Your number is too big. Please, try again.");
            else {
               int[] res =  Arrays.stream(allNumber).boxed()
                        .sorted(Comparator.reverseOrder())
                        .mapToInt(Integer::intValue)
                        .toArray();
                System.out.println(Arrays.toString(res));
                System.out.println("Congratulations," + name);
            }
        }


    }
}
