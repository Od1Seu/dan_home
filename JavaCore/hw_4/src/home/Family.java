package home;

import java.util.Arrays;

public class Family {
    private  Human mather;
    private Human father;
    private Human[] children;
    private int counter = 0;
    private static final int DEFAULT_LENGTH = 1;
    private Pet ket;

  public Family(Human mather, Human father,Pet ket){
      this.mather = mather;
      this.father = father;
      this.ket = ket;
      this.children = new Human[DEFAULT_LENGTH];
  }

  public Human addChild(Human child){
      if(counter*100/DEFAULT_LENGTH > 90){
       Human[] newArr = new Human[children.length + DEFAULT_LENGTH];
       System.arraycopy(children, 0, newArr,0,counter);
       children = newArr;
      };
      return this.children[counter++] = child;
  };

  public boolean deleteChild(int index){
      for (int i = 0; i < children.length; i++) {
          if (children[i] == children[index]) {
              children = removeElement(children,index);
              return true;
          }
      }
      return false;
  };

  public int countFamily(){
    return children.length + 2;
  };

    @Override
    public String toString() {
        return "Family{" +
                "mather=" + mather +
                ", father=" + father +
                ", children=" + Arrays.toString(getChildren()) +
                ", ket=" + ket +
                '}';
    }
    public Human getMather() {
        return mather;
    }
    public void setMather(Human mather) {
        this.mather = mather;
    }
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public Human[] getChildren() {
        return children;
    }
    public void setChildren(Human[] children) {
        this.children = children;
    }
    public Pet getKet() {
        return ket;
    }
    public void setKet(Pet ket) {
        this.ket = ket;
    }

    public static Human[] removeElement(Human[] main, int element ){
        Human[] arr = new Human[main.length-1];
        System.arraycopy(main,0,arr,0,element);
        System.arraycopy(main,element+1,arr,element,main.length-element-1);
        return arr;
    }
}
